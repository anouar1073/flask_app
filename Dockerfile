# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /python-docker

ENV FLASK_APP=flaskr
ENV FLASK_ENV =development

COPY Pipfile Pipfile
RUN cat Pipfile
RUN pip install pipenv
RUN pipenv install --deploy --ignore-pipfile

COPY . .

RUN export FLASK_APP=flaskr
RUN export FLASK_ENV=development
ENTRYPOINT [ "pipenv" ]
CMD ["run","python3", "-m" , "flask", "run", "--host=0.0.0.0"]