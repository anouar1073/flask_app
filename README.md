## Dockerize Backend
create the docker file in the current dirctory of the backend

```
# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /python-docker

ENV FLASK_APP=flaskr
ENV FLASK_ENV =development

COPY Pipfile Pipfile
RUN cat Pipfile
RUN pip install pipenv
RUN pipenv install --deploy --ignore-pipfile

COPY . .

RUN export FLASK_APP=flaskr
RUN export FLASK_ENV=development
ENTRYPOINT [ "pipenv" ]
CMD ["run","python3", "-m" , "flask", "run", "--host=0.0.0.0"]
```
Build the backend image

```
docker build --tag sample:flask-app .
```
run the backend container

```
docker run sample:flask-app
```
## Dockerize Frontend
create the docker file in the current dirctory of the frontend

```
# pull the official base image
FROM node:lts-alpine3.16
# set working direction
WORKDIR /app
# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
# install application dependencies
COPY package.json ./
COPY package-lock.json ./

COPY --chown=node:node package.json .

RUN yarn install
# add app

COPY --chown=node:node . .

USER node

# start app
CMD ["npm", "start"]
```
build the frontend image
```
docker build --tag sample:react-app .
```
run the backend container

```
docker run sample:react-app
```
## Docker-compose
we can use docker-compose to unify the build of the system images. For that, we add the docker-compose.yml file
```
version: "3"
services:
  backend:
    container_name: flask_backend
    image: flask-docker:latest
  frontend:
    container_name: frontend
    image: sample:react-app
    depends_on:
      - backend
```
then building and runing system containers is done with a single command
```
docker-compose -f docker-compose.yml up
```
## Publish image to Gitlab
Gitlab's container registry can be used to store the images that are created for the project. This process can be automated by integrating docker publishing with gitlab-ci. To do this, we add the following to gitlab-ci.yml file

```
build:
  image: docker:19.03.12
  stage: build
  services:
    - docker:19.03.12-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
```
